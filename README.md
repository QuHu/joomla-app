Cloudron-Joomla
===============
![Joomla](assets/joomla-logo.png)

## Local-Build
```bash
IMAGE=yourRegistryHere/joomla-cloudronapp:$(date +%N)
docker build --tag ${IMAGE} .
docker push ${IMAGE}
```

## Install
```bash
cloudron install --image ${IMAGE} --location joomla
```

__Follow POST installation steps!__
 
## Update
```bash
cloudron update --image ${IMAGE} --app joomla
```

## Debugging
```bash
cloudron logs --app joomla
cloudron exec --app joomla
```

## Uninstall
```bash
cloudron uninstall --app joomla
```

## Links
* https://www.joomla.org/
* https://docs.joomla.org/J4.x:Joomla_CLI_Installation
* https://github.com/joomla-docker/docker-joomla/blob/master/4.3/php8.2/apache/Dockerfile
* https://hub.docker.com/_/joomla
* https://docs.cloudron.io/
* https://github.com/opencontainers/image-spec/blob/main/annotations.md
