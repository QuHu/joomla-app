Joomla Post installation steps
==============================
⚠️ This app __not__ pre-setup with an admin account.

## Post installation steps
0. install the app
1. Use the __File Manager__ to edit `kickstart.ini`   
   1. Set email
   2. Set admin-username
   3. Set password - minimum 12 (twelve) characters 
2. restart the app 
3. done
 
ℹ️ Notes:
* Website is set to `Offline-Mode` initially
* no LDAP login available using native Joomla only

## LDAP authentication
Native Joomla 4 can only do authentication - but not authorization:    
No group filtering or mapping nor attribute sync is possible.

1. Fetch the Cloudron-LDAP values  
   (replace `myJoomla` with your app name)
    ```bash
   cloudron exec --app myJoomla printenv | grep CLOUDRON_LDAP
   ``` 
2. Login to Joomla administration backend
3. Enable LDAP plugin  
   → _System_ → _Plugins_ → Filter: `Authentication - LDAP` 
4. Configure LDAP plugin  
   Replace the `CLOUDRON_LDAP_XXX` placeholders
 
    | field                | value                         |
    |----------------------|-------------------------------|
    | Host                 | `CLOUDRON_LDAP_HOST`          |
    | Port                 | `CLOUDRON_LDAP_PORT`          |
    | LDAP V3              | _Yes_                         |
    | Connection Security  | _None_                        |
    | Follow Referrals     | _No_                          |
    | Authorisation Method | _Bind directly as User_       |
    | Base DN              | _ou=users,dc=cloudron_        |
    | Search String        | _username=[search]_           |
    | User's DN            | `CLOUDRON_LDAP_BIND_DN`       |
    | Connect Username     |                               |
    | Connect Password     | `CLOUDRON_LDAP_BIND_PASSWORD` |
    | Map: Full Name       | _displayName_                 |
    | Map: Email           | _mail_                        |
    | Map: User ID         | _username_                    |
    | Debug                | _No_                          |

5. Create new user in Joomla.  
   1. → _Users_ → _Manage_ → _New_  
   2. Fill mandatory fields `Name` and ´_Login Name (Username)_`  
   ⚠️ _Username_  **must** match a Cloudron-LDAP _userId_
   3. Assign Joomla desired user groups 
6. Login to Joomla with recently created `Username` using Cloudron-LDAP password 
