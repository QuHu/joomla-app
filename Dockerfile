ARG REGISTRY=${REGISTRY:-docker.io}
ARG BASEVERSION=${BASEVERSION:-4.1.0}
FROM ${REGISTRY}/cloudron/base:${BASEVERSION}

ENV JOOMLA_INSTALLATION_DISABLE_LOCALHOST_CHECK=1
ENV JOOMLA_VERSION=4.3.4
ENV JOOMLA_SHA512=2efede3c3230d2fd849c46f84eea9aa1ef2bef6836e0ceec6451499bb6abf7e747d63f3996d0a5f7fc2d439c5ae0380e0e71f7c2823adfdda80e87a0bc377be1

RUN mkdir -p  /app/pkg/

# Update and install dependencies
RUN set -ex ; \
    apt-get update ; \
    apt-get install --yes --no-install-recommends \
    ghostscript \
    libapache2-mod-php8.1 \
    php8.1-apcu \
    php8.1-bz2 \
    php8.1-redis \
    php8.1-xml \
    php8.1-soap \
    php8.1-mysql \
    php8.1-imagick \
    php8.1-oauth \
    php8.1-ldap \
    php8.1-gd \
    php8.1-cli \
    php8.1-zip \
    php8.1-mbstring \
    php8.1-bcmath \
    php8.1-memcached ; \
    apt-get clean; \
   apt-get purge --yes --auto-remove; \
   rm -rf /var/lib/apt/lists/*;

RUN set -ex ; \
	curl --output /app/pkg/joomla.tar.bz2 --fail --silent --show-error --location "https://github.com/joomla/joomla-cms/releases/download/${JOOMLA_VERSION}/Joomla_${JOOMLA_VERSION}-Stable-Full_Package.tar.bz2" ; \
    echo "${JOOMLA_SHA512} /app/pkg/joomla.tar.bz2" | sha512sum -c - ;

# MinSpareServers equals MaxSpareServers
RUN sed --expression="s,MaxSpareServers[^:].*,MaxSpareServers 5," --in-place /etc/apache2/mods-available/mpm_prefork.conf

RUN mkdir /tmp/ioncube && \
    curl --fail --silent --show-error --location http://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz | tar zxvf - -C /tmp/ioncube && \
    cp /tmp/ioncube/ioncube/ioncube_loader_lin_8.1.so /usr/lib/php/ && \
    rm --force --recursive /tmp/ioncube && \
    echo "zend_extension=/usr/lib/php/ioncube_loader_lin_8.1.so" > /etc/php/8.1/apache2/conf.d/00-ioncube.ini && \
    echo "zend_extension=/usr/lib/php/ioncube_loader_lin_8.1.so" > /etc/php/8.1/cli/conf.d/00-ioncube.ini

RUN set -ex ; \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.memory_consumption 128 ; \
    crudini --set /etc/php/8.1/apache2/php.ini opcache interned_strings_buffer 8 ; \
    crudini --set /etc/php/8.1/apache2/php.ini opcache max_accelerated_files 4000 ; \
    crudini --set /etc/php/8.1/apache2/php.ini opcache revalidate_freq 2 ; \
    crudini --set /etc/php/8.1/apache2/php.ini opcache fast_shutdown 1 ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 256M ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_size 256M ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 256M ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 256M ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_execution_time 200 ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP error_reporting 'E_ERROR | E_WARNING | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING | E_RECOVERABLE_ERROR' ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP display_errors off ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP log_errors on ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP error_log /dev/stderr ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP log_errors_max_len 1024  ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP ignore_repeated_errors on  ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP ignore_repeated_source off  ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP html_errors off ; \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_tmp_dir /var/tmp ; \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/app/sessions ; \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 ; \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100 ;

ADD assets/conf/trusted-proxies.lst /etc/apache2/conf-available/trusted-proxies.lst
ADD assets/conf/remoteip.conf       /etc/apache2/conf-available/remoteip.conf
ADD assets/conf/apache2.conf        /etc/apache2/conf-available/base.conf
ADD assets/conf/joomla-vhost.conf   /etc/apache2/sites-available/joomla.conf
ADD assets/conf/kickstart.ini /app/pkg/kickstart.ini
ADD assets/conf/index.html    /app/pkg/index.html

RUN set -ex ; \
    a2dismod status perl info; \
    a2enmod headers rewrite expires remoteip php8.1 ; \
    a2disconf other-vhosts-access-log ; \
    a2enconf remoteip base ; \
    a2dissite '*'; \
    a2ensite joomla ;

ADD start.sh /app/pkg/

EXPOSE 80/tcp

CMD [ "/app/pkg/start.sh" ]
