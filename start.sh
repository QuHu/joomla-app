#!/bin/bash

set -eu -o pipefail

status(){
    [[ $1 -gt 0 ]] && msg="\033[0;31m[FAIL]\033[0m" || msg="\033[0;32m[ OK ]\033[0m"
    printf "$msg"
}

validate_settings(){
    local adminEmail
    adminEmail="$(crudini --get /app/data/kickstart.ini '' admin_email)"
    [[ "$adminEmail" =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]
    local adminEmail_status=$?

    local adminUser
    adminUser="$(crudini --get /app/data/kickstart.ini '' admin_user)"
    [[ "$adminUser" != "" ]]
    local adminUser_status=$?

    local adminPassword
    adminPassword="$(crudini --get /app/data/kickstart.ini '' admin_password)"
    [[ ${#adminPassword} -ge 12 ]]
    local adminPassword_status=$?

    echo "$(status $adminEmail_status) adminEmail: ´$adminEmail´"
    echo "$(status $adminUser_status) adminUser: ´$adminUser´"
    echo "$(status $adminPassword_status) adminPassword length: ${#adminPassword}"
    if [[ $adminEmail_status -ne 0 ]] || [[ $adminUser_status -ne 0 ]] || [[ $adminPassword_status -ne 0 ]] ; then
        return 1
    else
        return 0
    fi
}

if [[ ! -f /app/data/.initialized && ! -f /app/data/webapp/configuration.php ]]; then
    # Init kickstart.ini if not exists
    if [[ ! -f /app/data/kickstart.ini ]] ; then
        cp /app/pkg/kickstart.ini /app/data/kickstart.ini
    fi

    mkdir -p /app/data/{webapp,tmp,logs}
    chown --recursive www-data:www-data /app/data/{webapp,tmp,logs}/

    # validate kickstart.ini
    set +e; validate_settings;  result=$? ; set -e ;

    if [[ $result -ne 0 ]] ; then
        echo "Validation Failed."
        cp /app/pkg/index.html /app/data/webapp/index.html
        touch /app/data/webapp/robots.txt
    else
        pushd  /app/data/webapp/

        echo "Validation Ok. Installing Joomla..."
        tar -xf /app/pkg/joomla.tar.bz2 -C /app/data/webapp/
        chown --recursive www-data:www-data /app/data/webapp/

        # Run Joomla installation
        # https://docs.joomla.org/J4.x:Joomla_CLI_Installation
        source /app/data/kickstart.ini

        # shellcheck disable=SC2154
        php installation/joomla.php install \
        --site-name="${CLOUDRON_APP_DOMAIN}" \
        --admin-user=Joomla-Admin \
        --admin-username="${admin_user}" \
        --admin-password="${admin_password}" \
        --admin-email="${admin_email}" \
        --db-type=mysqli \
        --db-host="${CLOUDRON_MYSQL_HOST}" \
        --db-user="${CLOUDRON_MYSQL_USERNAME}" \
        --db-pass="${CLOUDRON_MYSQL_PASSWORD}" \
        --db-name="${CLOUDRON_MYSQL_DATABASE}" \
        --db-encryption=0 \
        --db-prefix=cj_ \
        --no-interaction

        chown www-data:www-data /app/data/webapp/configuration.php
        rm --recursive /app/data/kickstart.ini
        echo "Initialized $(date +'%F %T')" > /app/data/.initialized
        rm --recursive /app/data/webapp/index.html

        # Post Configuration
        # https://docs.joomla.org/J4.x:CLI_Update#Configuration_based_commands
        php cli/joomla.php site:down
        mv htaccess.txt .htaccess

        # Basics
        php cli/joomla.php config:set cors=false ;
        php cli/joomla.php config:set behind_loadbalancer=true
        php cli/joomla.php config:set sef_rewrite=true ;
        php cli/joomla.php config:set tmp_path=/app/data/tmp ;
        php cli/joomla.php config:set gzip=true ;
        php cli/joomla.php config:set mailonline=true ;

        # Caching
        php cli/joomla.php config:set cache_handler=redis ;
        php cli/joomla.php config:set redis_server_auth="${CLOUDRON_REDIS_PASSWORD}" ;
        php cli/joomla.php config:set redis_server_host="${CLOUDRON_REDIS_HOST}" ;
        php cli/joomla.php config:set redis_server_host="${CLOUDRON_REDIS_HOST}" ;
        php cli/joomla.php config:set redis_server_db=0 ;
        php cli/joomla.php config:set redis_persist=true ;

        # Session handling
        php cli/joomla.php config:set session_metadata=true ;
        php cli/joomla.php config:set session_redis_server_host="${CLOUDRON_REDIS_HOST}" ;
        php cli/joomla.php config:set session_redis_server_port="${CLOUDRON_REDIS_PORT}" ;
        php cli/joomla.php config:set session_redis_server_auth="${CLOUDRON_REDIS_PASSWORD}" ;
        php cli/joomla.php config:set session_redis_persist=1 ;
        php cli/joomla.php config:set session_redis_server_db=1 ;
        php cli/joomla.php config:set session_metadata_for_guest=true ;
        php cli/joomla.php config:set session_handler=redis ;

        # Mail
        if [[ -v CLOUDRON_MAIL_DOMAIN ]] ; then
            php cli/joomla.php config:set mailer=smtp ;
            php cli/joomla.php config:set mailfrom="webmaster@${CLOUDRON_MAIL_DOMAIN}" ;
            php cli/joomla.php config:set smtphost="${CLOUDRON_MAIL_SMTP_SERVER}" ;
            php cli/joomla.php config:set smtpauth=true ;
            php cli/joomla.php config:set smtpuser="${CLOUDRON_MAIL_SMTP_USERNAME}" ;
            php cli/joomla.php config:set smtppass="${CLOUDRON_MAIL_SMTP_PASSWORD}" ;
            php cli/joomla.php config:set smtpport="${CLOUDRON_MAIL_SMTP_PORT}" ;
            php cli/joomla.php config:set smtpsecure=none ;
        fi
    fi
fi

mkdir -p /run/php/sessions
chown www-data:www-data /run/php/sessions

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm --force "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
