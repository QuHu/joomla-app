# Changelog

[![Common Changelog](https://common-changelog.org/badge.svg)](https://common-changelog.org)
[![Static Badge](https://img.shields.io/badge/2.0.0-blue?label=semver&cacheSeconds=5)](https://semver.org/spec/v2.0.0.html)

## [0.0.1-alpha-1]() - 2023-09-26

### Added
- initial Version
